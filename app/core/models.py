from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import MaxValueValidator, MinValueValidator

from .managers import UserManager


class InstitutionType(models.Model):
    name = models.CharField('Institution type', max_length=200, unique=True)

    def __str__(self):
        return self.name


class Institution(models.Model):
    name = models.CharField('Institution name', max_length=200, unique=True)
    type = models.ForeignKey(InstitutionType,
                             on_delete=models.SET_NULL,
                             null=True,
                             blank=True)
    email = models.EmailField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email


class Stream(models.Model):
    stream_name = models.CharField('Stream', max_length=200, unique=True)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)

    def __str__(self):
        return self.stream_name


class Division(models.Model):
    division_name = models.CharField('Division', max_length=30, unique=True)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)

    def __str__(self):
        return self.division_name


class SessionName(models.Model):
    session_title = models.CharField('Session title', max_length=150,
                                     blank=False)
    is_interval = models.BooleanField(default=False)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)

    def __str__(self):
        return self.session_title


class Subject(models.Model):
    subject_name = models.CharField('subject name', max_length=100)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)
    type_choices = [
        ('1', 'Theory'),
        ('2', 'Practical'),
        ('3', 'Student Activity')
    ]
    preference = models.ForeignKey(SessionName,
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True)
    subject_type = models.CharField('Type', choices=type_choices,
                                    max_length=20, default=type_choices[0][0])
    stream = models.ForeignKey(Stream, on_delete=models.SET_NULL, null=True)
    division = models.ForeignKey(Division, on_delete=models.SET_NULL,
                                 null=True)
    continuous_session = models.PositiveSmallIntegerField(
                            'Continous sessions required',
                            default=1,
                            validators=[
                                MaxValueValidator(6),
                                MinValueValidator(1)
                            ])
    weekly_hours = models.PositiveSmallIntegerField(
                            'Weekly hours required',
                            default=1,
                            validators=[
                                MaxValueValidator(56),
                                MinValueValidator(1)
                            ])

    class Meta:
        unique_together = [['stream', 'division', 'subject_name']]

    def __str__(self):
        return self.subject_name


class Expertise(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             related_name="expertise")
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    proficiency_choice = [
        ('1', 'Expert'),
        ('2', 'Intermediate'),
        ('3', 'Beginner'),
        ('4', 'newbie')
    ]
    proficiency = models.CharField('Proficiency',
                                   choices=proficiency_choice,
                                   max_length=20,
                                   default=proficiency_choice[3][0])

    def __str__(self):
        return self.subject.subject_name

    class Meta:
        unique_together = [['user', 'subject']]


class UserProfile(models.Model):
    user = models.OneToOneField(
            User,
            on_delete=models.CASCADE,
            related_name='profile'
            )
    first_name = models.CharField('First name', max_length=150, blank=True)
    last_name = models.CharField('Last name', max_length=150, blank=True)
    weekly_hours = models.PositiveSmallIntegerField(
                            'Weekly available hours',
                            default=1,
                            validators=[
                                MaxValueValidator(48),
                                MinValueValidator(1)
                            ])
    institution = models.ForeignKey(Institution, on_delete=models.SET_NULL,
                                    null=True,
                                    blank=True)


class ClassSession(models.Model):
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)
    session_title = models.ForeignKey(SessionName, on_delete=models.CASCADE)
    session_subtitle = models.CharField('Session subtitle', max_length=150,
                                        blank=False)
    start_time = models.TimeField('Start time', blank=False)
    end_time = models.TimeField('End time', blank=False)
    order_id = models.PositiveSmallIntegerField(
                            'Session order',
                            default=1,
                            validators=[
                                MaxValueValidator(20),
                                MinValueValidator(1)
                            ])

    def __str__(self):
        return self.session_subtitle


class DayDetails(models.Model):
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE,
                                    null=True)
    day_name = models.CharField('Day', max_length=50, blank=False)
    order_id = models.PositiveSmallIntegerField(
                            'Session order',
                            default=1,
                            validators=[
                                MaxValueValidator(20),
                                MinValueValidator(1)
                            ])
    sessions = models.ManyToManyField(ClassSession)


class InstitutionRequest(models.Model):
    from_user = models.ForeignKey(User,
                                  on_delete=models.CASCADE)
    name = models.CharField('Institution name', max_length=200)
    type = models.CharField('Institution type', max_length=200)
    email = models.EmailField(max_length=255)
    message = models.CharField('Institution type', max_length=1000, blank=True)

    def __str__(self):
        return self.message
