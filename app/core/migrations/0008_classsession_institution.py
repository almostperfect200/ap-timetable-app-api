# Generated by Django 3.1.7 on 2021-02-28 07:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_subject_institution'),
    ]

    operations = [
        migrations.AddField(
            model_name='classsession',
            name='institution',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.institution'),
        ),
    ]
