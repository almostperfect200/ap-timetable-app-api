from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.forms.models import ModelChoiceField
from django.urls import reverse
from django.utils.http import urlencode
from django.utils.html import format_html

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User, Subject, Stream, Division, Expertise, UserProfile,\
                    ClassSession, SessionName, DayDetails, InstitutionRequest,\
                    Institution, InstitutionType


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ('email', 'is_staff', 'is_active', 'view_profile')
    list_filter = ('email', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff',
                       'is_active')}),
    )
    search_fields = ('email',)
    ordering = ('email',)

    def view_profile(self, obj):
        url = 'http://localhost:8000/admin/core/userprofile/{}/change/'\
                        .format(str(obj.profile.id))
        return format_html('<a href="{}">View profile</a>', url)

    view_profile.short_description = "Profile"


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('subject_name', 'stream', 'division', 'weekly_hours')
    list_filter = ('stream',)
    search_fields = ('subject_name__startswith',)

    class Meta:
        ordering = ('stream', 'subject_name')


class ProfileAdmin(admin.ModelAdmin):
    profile_id = None
    list_display = ('first_name', 'last_name', 'user')

    def has_add_permission(self, request):
        return False

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.profile_id = obj.id
        return super(ProfileAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'expertise':
            user_profile = UserProfile.objects.get(id=self.profile_id)
            queryset = Expertise.objects.filter(user=user_profile.user)
            return ModelChoiceField(queryset, initial=[('', '-----------')])
        else:
            return super(ProfileAdmin, self).formfield_for_foreignkey(
                db_field,
                request,
                **kwargs
            )


class SessionNameAdmin(admin.ModelAdmin):
    list_display = ('session_title', 'is_interval')


class ClassSessionAdmin(admin.ModelAdmin):
    list_display = ('order_id', 'session_subtitle', 'session_title', 'start_time',
                    'end_time')

    class Meta:
        ordering = ('order_id')


class DayDetailsAdmin(admin.ModelAdmin):
    list_display = ('day_name',)


admin.site.register(User, CustomUserAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Stream)
admin.site.register(Division)
admin.site.register(Expertise)
admin.site.register(UserProfile, ProfileAdmin)
admin.site.register(ClassSession, ClassSessionAdmin)
admin.site.register(SessionName, SessionNameAdmin)
admin.site.register(DayDetails, DayDetailsAdmin)
admin.site.register(InstitutionRequest)
admin.site.register(Institution)
admin.site.register(InstitutionType)
