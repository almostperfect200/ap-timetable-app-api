from django.dispatch import receiver
from django.db.models.signals import post_save
from .models import UserProfile, User
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Token.objects.create(user=instance)
        profile = UserProfile(user=instance)
        profile.save()
