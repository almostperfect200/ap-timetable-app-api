from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.settings import api_settings
from rest_framework.authtoken.models import Token

from user.serializers import UserSerializer, AuthTokenSerializer, \
                                ProfileSerializer, UserExpertiseSerilizer, \
                                InstitutionRequestSerializer

from core.models import UserProfile, Expertise, User, InstitutionRequest


@api_view(['POST', ])
def registration_view(request):

    if request.method == 'POST':
        serializer = UserSerializer(data=request.data)
        data = {}
        if serializer.is_valid():
            account = serializer.save()
            data['response'] = "Success"
            data['email'] = account.email
            token = Token.objects.get(user=account).key
            data['token'] = token
        else:
            data = serializer.errors
        return Response(data)


@api_view(['POST', 'GET', 'DELETE'])
@permission_classes((IsAuthenticated, ))
def institution_message(request):
    owner = request.user
    if request.method == 'POST':
        data = {'from_user': owner.id,
                'name': request.data.get('name'),
                'type': request.data.get('type'),
                'email': request.data.get('email'),
                'message': request.data.get('message')}
        serializer = InstitutionRequestSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        messages = InstitutionRequest.objects.filter(from_user=owner.id)
        return Response({'serializer.data': list(messages.values())})
    elif request.method == 'DELETE':
        id = request.data.get('id')
        try:
            obj = InstitutionRequest.objects.get(id=id)
        except InstitutionRequest.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj.delete()
        return Response({'data': 'deleted'})


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
def profile_view(request):
    owner = request.user
    try:
        profile = UserProfile.objects.get(user=owner)
    except UserProfile.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProfileSerializer(profile)
        return Response(serializer.data)


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
def expertise_view(request):
    owner = request.user
    try:
        expertise = User.objects.get(id=owner.id)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserExpertiseSerilizer(expertise)
        return Response(serializer.data)


class CreateTokenView(ObtainAuthToken):
    """Create a new auth token for the user"""
    serializer_class = AuthTokenSerializer
    renderer_classes = api_settings.DEFAULT_RENDERER_CLASSES
