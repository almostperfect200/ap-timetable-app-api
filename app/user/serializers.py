from django.contrib.auth import get_user_model, authenticate
from core.models import UserProfile, Expertise, InstitutionRequest

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = ('email', 'password')
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}

    def create(self, validated_data):
        return get_user_model().objects.create_user(**validated_data)


class ExpertiseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expertise
        fields = ('subject', 'proficiency')


class ProficiencyListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expertise
        fields = ('proficiency_choice')


class InstitutionRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = InstitutionRequest
        fields = ('from_user', 'name', 'type', 'email', 'message')


class UserExpertiseSerilizer(serializers.ModelSerializer):

    expertise = ExpertiseSerializer(many=True, read_only=True)
    proficiency_choice = serializers.\
        SerializerMethodField('get_proficiency_list')

    def get_proficiency_list(self, expertise):
        return [
            ('1', 'Expert'),
            ('2', 'Intermediate'),
            ('3', 'Beginner'),
            ('4', 'newbie')
        ]

    class Meta:
        model = get_user_model()
        fields = ('email', 'expertise', 'proficiency_choice')


class ProfileSerializer(serializers.ModelSerializer):

    email = serializers.SerializerMethodField('get_user_details')

    class Meta:
        model = UserProfile
        fields = ('first_name', 'last_name', 'email',
                                'weekly_hours')

    def get_user_details(self, profile):
        email = profile.user.email
        return email


class AuthTokenSerializer(serializers.Serializer):
    """Serializer for the user authentication object"""
    email = serializers.CharField()
    password = serializers.CharField(
        style={'input_type': 'password'},
        trim_whitespace=False
    )

    def validate(self, attrs):
        """Validate and authenticate the user"""
        email = attrs.get('email')
        password = attrs.get('password')

        user = authenticate(
            request=self.context.get('request'),
            username=email,
            password=password
        )
        if not user:
            msg = 'Unable to authenticate with provided credentials'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
