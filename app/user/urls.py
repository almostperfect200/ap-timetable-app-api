from django.urls import path

from user import views

app_name = 'user'

urlpatterns = [
    path('create/', views.registration_view, name='create'),
    path('token/', views.CreateTokenView.as_view(), name='token'),
    path('profile/', views.profile_view, name='profile'),
    path('expertise/', views.expertise_view, name='expertise'),
    path('institution_message/', views.institution_message,
         name='institution message'),
]
