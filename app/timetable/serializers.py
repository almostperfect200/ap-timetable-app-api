
from rest_framework import serializers
from timetable.tta_logic import *


class_room_crse_list = {'C01': ['S01', 'S02', 'S03', 'S04', 'S05',
                        'S06', 'S07']}

lect_details = {'L01': ['1', 'L01', ['S01', 'S07'], 30, 6],
                'L02': ['2', 'L02', ['S02', 'S07'], 30, 6],
                'L03': ['3', 'L03', ['S03', 'S07'], 30, 6],
                'L04': ['4', 'L04', ['S04', 'S07'], 30, 6],
                'L05': ['5', 'L05', ['S05', 'S07'], 30, 6],
                'L06': ['6', 'L06', ['S06', 'S07'], 30, 6],
                'L07': ['7', 'L07', ['S01', 'S07'], 30, 6]}

plan_details = {'1': ['1', ['1', '2'], [3, 3]],
                '2': ['2', ['1', '2'], [3, 3]],
                '3': ['3', ['1', '2'], [3, 3]],
                '4': ['4', ['1', '2'], [3, 3]],
                '5': ['5', ['1', '2'], [3, 3]]}

crse_details = {'S01': ['1', 'S01', 'class room', 5, 1, 0],
                'S02': ['2', 'S02', 'class room', 5, 1, 0],
                'S03': ['3', 'S03', 'class room', 5, 1, 0],
                'S04': ['4', 'S04', 'class room', 4, 1, 0],
                'S05': ['5', 'S05', 'class room', 4, 1, 0],
                'S06': ['6', 'S06', 'class room', 4, 1, 0],
                'S07': ['7', 'S07', 'lab', 1, 3, 0]}


class TimetableSerializer(serializers.Serializer):

    result = solve_TTS_problem(class_room_crse_list, lect_details, plan_details, crse_details)
    print(result)
    details = serializers.IntegerField()
