from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from core.models import Subject, Expertise, User, DayDetails, SessionName
from timetable.tta_logic import solve_TTS_problem


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
def timetable_view(request):
    owner = request.user
    if request.method == 'GET':
        request_params = request.GET
        try:
            stream_id = int(request_params.get('stream_id', default=None))
            division_id = int(request_params.get('division_id', default=None))
        except Exception as ex:
            return Response(TtaQuery(0, str(ex)).response)
        try:
            profile = owner.profile
        except Exception as ex:
            return Response(TtaQuery(0, str(ex)).response)
        institution_id = profile.institution
        if institution_id is None:
            return Response(TtaQuery(0, 'No institution found').response)
        if stream_id is None or type(stream_id) != int:
            return Response(TtaQuery(0, 'missing stream_id').response)
        elif division_id is None or type(stream_id) != int:
            return Response(TtaQuery(0, 'missing division').response)
        else:
            subjects = Subject.objects.filter(stream=stream_id,
                                              division=division_id,
                                              institution=institution_id)
            if subjects:
                subject_list = courseList(subjects)
                class_room_crse_list_new = {f'{stream_id}_{division_id}':
                                            subject_list}
                lect_details_new = lecturerList(subjects).response
                new_plan_details = newPlanDetails(institution_id)
                crse_details_new = courseDetails(subjects).response
                solution = solve_TTS_problem(
                                            class_room_crse_list_new,
                                            lect_details_new,
                                            new_plan_details,
                                            crse_details_new
                                             )

                print("*****************")
                print("Class room course list")
                print("*****************")
                print(class_room_crse_list_new)
                print("*****************")

                print("*****************")
                print("Lecturer Details")
                print("*****************")
                print(lect_details_new)
                print("*****************")

                print("*****************")
                print("Plan details")
                print("*****************")
                print(new_plan_details)
                print("*****************")

                print("*****************")
                print("Course details")
                print("*****************")
                print(crse_details_new)
                print("*****************")

                print("*****************")
                print("Solution")
                print("*****************")
                print(solution)
                print("*****************")
                return Response(TtaQuery(1, solution).response)
                # return Response(TtaQuery(1, solution).response)
            else:
                return Response(TtaQuery(0, 'subjects missing').response)


def courseList(subjects):
    subject_list = []
    for subject in subjects:
        subject_list.append(f'{subject.subject_name}')
    return subject_list


def lecturerList(subjects):
    subject_list = []
    for subject in subjects:
        subject_list.append(f'{subject.id}')
    response = {}
    users = User.objects.all()
    if users:  # has users in the system
        for user in users:
            expertises = Expertise.objects.filter(user=user.id,
                                                  subject__in=subject_list)
            if expertises:  # user has expertise
                subjects = []
                for expertise in expertises:
                    subjects.append(f'{expertise.subject.subject_name}')
                response[
                    f'{user.profile.first_name} {user.profile.last_name}'
                    ] = [
                            None,
                            f'{user.profile.first_name} {user.profile.last_name}',
                            subjects,
                            user.profile.weekly_hours,
                            4
                        ]
        return TtaQuery(1, response)
    else:
        return TtaQuery(0, 'No Users')


def planDetails():
    response = {'Monday': ['Monday', ['Forenoon', 'Afternoon'], [3, 3]],
                'Tuesday': ['Tuesday', ['Forenoon', 'Afternoon'], [3, 3]],
                'Wednesday': ['Wednesday', ['Forenoon', 'Afternoon'], [3, 3]],
                'Thursday': ['Thursday', ['Forenoon', 'Afternoon'], [3, 3]],
                'Friday': ['Friday', ['Forenoon', 'Afternoon'], [3, 3]]}
    return response


def newPlanDetails(institution_id):
    response = {}
    day_objects = DayDetails.objects.filter(institution=institution_id)
    session_details = SessionName.objects.filter(is_interval=False,
                                                 institution=institution_id)
    for day_object in day_objects:
        session_names = []
        session_number = []
        for session_info in session_details:
            sessions = day_object.sessions.filter(
                session_title__session_title=f'{session_info.session_title}')
            if len(sessions) > 0:
                session_names.append(session_info.session_title)
                session_number.append(len(sessions))
        if session_names and session_number:
            response[f'{day_object.day_name}'] = [f'{day_object.day_name}',
                                                  session_names,
                                                  session_number]
    return response


def courseDetails(subjects):
    response = {}
    for subject in subjects:
        preference = subject.preference
        preference_code = 0
        if preference is not None:
            preference_code = preference.session_title
        response[f'{subject.subject_name}'] = [
                                    None, f'{subject.subject_name}',
                                    f'{subject.subject_type}',
                                    subject.weekly_hours,
                                    subject.continuous_session,
                                    preference_code
                                    ]
    return TtaQuery(1, response)


class TtaQuery:
    def __init__(self, status, response, key=""):
        self.status = status
        if status == 0:
            self.response = {'error': response}
        else:
            self.response = response
