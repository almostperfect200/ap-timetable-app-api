# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 21:31:38 2021

@author: rakes
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Sep 26 16:14:22 2020

@author: rakes
"""


# Get objects from application.
def get_input_data(class_room_crse_list,
                   lect_details,
                   plan_details,
                   crse_details):
    '''
    Inputs:

    # class_room_crse_list: type-dict();
    #        key - classroom id (string),
    #        value - list of subject ids available for the class room
    #                (subject ids-string)
    #  ex: {'class_room_id': ['subject_1','subject2']}

    # lect_details: type-dict();
    #        key - lecturer id (string),
    #        value - list, size of the list = 5
    #           list elements:
    #               list[0] - lect index or None
    #               list[1] - lect id (string)
    #               list[2] - list of subjects taken by lecturer,
    #                         (subject ids-string)
    #               list[3] - weekly available hours/periods (integer)
    #               list[4] - daily available hours/periods (integer)
    #  ex: {'lecturer_id': [None, 'lecturer_id', ['subject_1','subject2'],
    #                       wkly_av_hrs, daily_av_hrs]}

    # plan_details: type-dict();
    #        key - day (string),
    #        value - list, size of the list = 3
    #           list elements:
    #               list[0] - day (string)
    #               list[1] - list of available sessions in the day
    #                         ( session ids string)
    #               list[2] - list available hours per session,(hours- integer)
    #  ex: {'day': [day, ['session_1','session_2'],
    #                       [available_hrs_session_1, available_hrs_session_2]}

    # crse_details: type-dict();
    #        key - course id (string),
    #        value - list, size of the list = 6
    #           list elements:
    #               list[0] - course index or None
    #               list[1] - course id (string)
    #               list[2] - course type (string)
    #               list[3] - weekly rquired hours/periods (integer)
    #               list[4] - continous hours/periods required per session
    #                         (integer)
    #               list[5] - session id (integer or string),if prefered
    #                         session availabe;
    #                         zero (integer), else.
    #  ex: {'course_id': [None, 'course_id', 'course_type', wkly_reqd_hrs,
    #                       cont_hrs_reqd, session_id]}


    '''
    # Extract class room details
    class_rooms = []
    for c in class_room_crse_list:
        class_rooms.append(c)

    # Extract lectures details
    lectures = []
    lect_crse_ofrd = {}
    for l in lect_details:
        lectures.append(l)
        lect_crse_ofrd[l] = lect_details[l][2][:]

    days = []
    day_sess = {}
    for day in plan_details:
        sess = plan_details[day][1]
        days.append(day)
        day_sess[day] = sess

    courses = []
    crse_types = []
    for crse in crse_details:
        typ = crse_details[crse][2]
        courses.append(crse)
        if typ not in crse_types:
            crse_types.append(typ)

    # Set available pds for each session.
    day_sess_pds = {}
    for d in days:
        start = 1
        #max_hrs = sum(h for h in plan_details[d][2])
        for sess_idx in range(len(plan_details[d][1])):
            sess = plan_details[d][1][sess_idx]
            hrs_av = plan_details[d][2][sess_idx]
            start_idx = start
            end_idx = start_idx + hrs_av
            pds = [t for t in range(start_idx,end_idx)]
            day_sess_pds[d,sess] = pds
            start += hrs_av

    ## Objects reqd for the model.
    prm_lect_crse = {(i,j):1 for i in lect_crse_ofrd
                         for j in lect_crse_ofrd[i]}

    lab_opps = {}
    lab_opps_pds = {}
    op = 1
    for l in class_rooms:
        for j in class_room_crse_list[l]:
            dur = crse_details[j][4]
            if  dur <= 1:
                continue
            for i in lectures:
                if not prm_lect_crse.get((i,j),0):
                    continue
                req_sess = crse_details[j][5]
                for d in days:
                    lab_opps[(l,i,j,d)] = []
                    if req_sess:
                        for sess in day_sess[d]:
                            if sess != req_sess:
                                continue
                            if len(day_sess_pds[d,sess]) < dur:
                                continue
                            for k in range(len(day_sess_pds[d,sess])-dur+1):
                                ops = [i for i in range(k,k+dur)]
                                lab_opps_pds[op] = [day_sess_pds[d,sess][i]
                                                        for i in ops]

                                lab_opps[(l,i,j,d)].append(op)
                                op += 1

                    else:
                        for sess in day_sess[d]:
                            if len(day_sess_pds[d,sess]) < dur:
                                continue
                            for k in range(len(day_sess_pds[d,sess])-dur+1):
                                ops = [i for i in range(k,k+dur)]
                                lab_opps_pds[op] = [day_sess_pds[d,sess][i]
                                                        for i in ops]

                                lab_opps[(l,i,j,d)].append(op)
                                op += 1



    return  class_rooms,lectures,lect_crse_ofrd,days,day_sess,courses,\
            crse_types,day_sess_pds,prm_lect_crse,lab_opps,lab_opps_pds




def optimize_TTS_problem(class_room_crse_list,lect_details,
                         plan_details,crse_details,
                         class_rooms,lectures,lect_crse_ofrd,
                         days,day_sess,courses,
                         crse_types,day_sess_pds,prm_lect_crse,
                         lab_opps,lab_opps_pds):

    import pulp
    mod = pulp.LpProblem('TTS',pulp.LpMinimize)
    # Decsn vars.
    mod.z_vars = pulp.LpVariable.dicts('Z',[(i,j,k,l,t) for i in lectures
                                    for l in class_rooms
                                    for j in class_room_crse_list[l]
                                    for k in days
                                    for sess in day_sess[k]
                                    for t in day_sess_pds[k,sess]],
                                lowBound=0,upBound=1,cat=pulp.LpBinary)

    mod.x_vars = pulp.LpVariable.dicts('X',[(i,k) for i in lectures for k in days],
                                  lowBound=0,upBound=1,cat=pulp.LpBinary)
    mod.lab_vars = pulp.LpVariable.dicts('Y',[(i[0],i[1],i[2],i[3],j)
                                     for i in lab_opps
                                     for j in lab_opps[i]],
                                     lowBound=0,upBound=1,
                                cat=pulp.LpBinary)

    mod += 0#lpSum(mod.x_vars[i,k] for i in lectures for k in days)


    # Ensures the rqd weekly crse hrs are met.
    for l in class_rooms:
        for j in class_room_crse_list[l]:
            mod += pulp.lpSum(mod.z_vars[i,j,k,l,t] for i in lectures
                                 for k in days
                                 for sess in day_sess[k]
                                 for t in day_sess_pds[k,sess])==\
                                 crse_details[j][3]*crse_details[j][4],\
                                 'ct1a_%s_%s'%(l,j)
    # Ensures the rqd weekly sessions are met for subjects with more than one pd
    # reqd contionisly.
    for l in class_rooms:
        for j in class_room_crse_list[l]:
            dur = crse_details[j][4]
            if  dur <= 1:
                continue
            mod += pulp.lpSum(mod.lab_vars.get((l,i,j,d,o),0)
                                for i in lectures for d in days
                                for o in lab_opps.get((l,i,j,d),[])) == \
                                crse_details[j][3],'ct1b_%s_%s'%(l,j)
    # Links mod.z_vars and mod.lab_vars
    for l in class_rooms:
        for j in class_room_crse_list[l]:
            dur = crse_details[j][4]
            if  dur <= 1:
                continue
            for i in lectures:
                if prm_lect_crse.get((i,j),0):
                    for k in days:
                        for o in lab_opps.get((l,i,j,k),[]):
                            for t in lab_opps_pds[o]:
                                mod += mod.z_vars[i,j,k,l,t]\
                                    >= mod.lab_vars.get((l,i,j,k,o),0)\
                                    ,'ct2_%s_%s_%s_%s_%s_%s'%(l,j,i,k,o,t)

    # Ensures lecturer can teach only if lecturer is available on day k and lecturer
    # teaches at most one subject at any time slot.


    # At each class room at most one course can be presented at any time.
    for l in class_rooms:
        for k in days:
            for sess in day_sess[k]:
                for t in day_sess_pds[k,sess]:
                    mod += pulp.lpSum(mod.z_vars[i,j,k,l,t] for i in lectures
                             for j in class_room_crse_list[l])<=1

    # Each lecturer is assigned to courses that he can teach
    for i in lectures:
        for j in courses:
            mod += pulp.lpSum(mod.z_vars[i,j,k,l,t] for l in class_rooms \
                          for k in days for sess in day_sess[k] \
                          for t in day_sess_pds[k,sess]) \
                            <= prm_lect_crse.get((i,j),0)*10*lect_details[i][3]

    # Max available hours for lecturer per week
    for i in lectures:
        mod += pulp.lpSum(mod.z_vars.get((i,j,k,l,t),0) for l in class_rooms \
                          for j in courses \
                          for k in days for sess in day_sess[k] \
                          for t in day_sess_pds[k,sess]) \
                            <= lect_details[i][3]

    # Max available hours for lecturer per day
    for i in lectures:
        for k in days:
            mod += pulp.lpSum(mod.z_vars.get((i,j,k,l,t),0) for l in class_rooms \
                          for j in courses \
                          for sess in day_sess[k] \
                          for t in day_sess_pds[k,sess]) \
                            <= lect_details[i][4]

    # Link lect availablity with mod.z_vars
    for i in lectures:
        for k in days:
            for sess in day_sess[k]:
                for t in day_sess_pds[k,sess]:
                    mod += pulp.lpSum(mod.z_vars[i,j,k,l,t] for j in courses
                             for l in class_rooms) <= mod.x_vars[i,k]


    #mod.writeLP('tts.lp')
    mod.solve()
    print("Status:", pulp.LpStatus[mod.status])
    print("Objective Value = ", pulp.value(mod.objective))
    return mod



def export_results(class_rooms,days,day_sess,day_sess_pds,
                   lectures,courses,mod):
    '''
    #### Result out put format
    # Output
    #   result_out: type dict()
    # ex: {classroom_id:
    #           {day:
    #               {sess: [(period,subject_id,lecturer_id)]
    #                     }}}
    '''
    result_out = {}
    for l in class_rooms:
        result_out[l] = {}
        for d in days:
            result_out[l][d] = {}
            for sess in day_sess[d]:
                result_out[l][d][sess] = []
                for t in day_sess_pds[d,sess]:
                    for i in lectures:
                        for j in courses:
                            v = (i,j,d,l,t)
                            if v not in mod.z_vars:
                                continue
                            if mod.z_vars[v].varValue >= 0.9:
                                result_out[l][d][sess].append((t,j,i))
    return result_out


class_room_crse_list = {'C01': ['S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07']}

lect_details = {'L01': ['1', 'L01', ['S01', 'S07'], 30, 6],
                 'L02': ['2', 'L02', ['S02', 'S07'], 30, 6],
                 'L03': ['3', 'L03', ['S03', 'S07'], 30, 6],
                 'L04': ['4', 'L04', ['S04', 'S07'], 30, 6],
                 'L05': ['5', 'L05', ['S05', 'S07'], 30, 6],
                 'L06': ['6', 'L06', ['S06', 'S07'], 30, 6],
                 'L07': ['7', 'L07', ['S01', 'S07'], 30, 6]}

plan_details = {'1': ['1', ['1', '2'], [3, 3]],
             '2': ['2', ['1', '2'], [3, 3]],
             '3': ['3', ['1', '2'], [3, 3]],
             '4': ['4', ['1', '2'], [3, 3]],
             '5': ['5', ['1', '2'], [3, 3]]}

crse_details = {'S01': ['1', 'S01', 'class room', 5, 1, 0],
                 'S02': ['2', 'S02', 'class room', 5, 1, 0],
                 'S03': ['3', 'S03', 'class room', 5, 1, 0],
                 'S04': ['4', 'S04', 'class room', 4, 1, 0],
                 'S05': ['5', 'S05', 'class room', 4, 1, 0],
                 'S06': ['6', 'S06', 'class room', 4, 1, 0],
                 'S07': ['7', 'S07', 'lab', 1, 3, 0]}


def solve_TTS_problem(class_room_crse_list,lect_details,
                      plan_details,crse_details):


    # get input data from application and setup objects and prms.
    class_rooms,lectures,lect_crse_ofrd,days,day_sess,courses,\
    crse_types,day_sess_pds,prm_lect_crse,\
        lab_opps,lab_opps_pds = get_input_data(class_room_crse_list,
                                                     lect_details,
                                                     plan_details,
                                                     crse_details)
    # Solve the TTS prblem
    mod = optimize_TTS_problem(class_room_crse_list,lect_details,
                                 plan_details,crse_details,
                                 class_rooms,lectures,lect_crse_ofrd,
                                 days,day_sess,courses,
                                 crse_types,day_sess_pds,prm_lect_crse,
                                 lab_opps,lab_opps_pds)

    result = export_results(class_rooms,days,day_sess,day_sess_pds,
                       lectures,courses,mod)
    return result
