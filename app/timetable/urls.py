from django.urls import path

from timetable import views

app_name = 'timetable'

urlpatterns = [
    path('create/', views.timetable_view, name='create'),
]
